package com.example.aggregate.services;

import com.example.aggregate.dtos.PersonBasicInfoDto;
import com.example.aggregate.dtos.PersonCompleteRecordDto;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Optional;

public interface UserManagementService {

    Optional<PersonBasicInfoDto> getPersonById(Long id) throws JsonProcessingException;

    Optional<PersonCompleteRecordDto> getPersonCompleteRecordById(Long id) throws JsonProcessingException;
}
