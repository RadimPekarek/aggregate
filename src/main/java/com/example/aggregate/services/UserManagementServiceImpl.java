package com.example.aggregate.services;

import com.example.aggregate.clients.UserManagementClient;
import com.example.aggregate.dtos.PersonBasicInfoDto;
import com.example.aggregate.dtos.PersonCompleteRecordDto;
import com.example.aggregate.rest.exceptions.models.ApiError;
import com.example.aggregate.services.exceptions.ServiceLayerForwardingException;
import com.example.aggregate.services.utils.HttpClientErrorExceptionMessageDeserializer;
import com.example.aggregate.services.utils.HttpServerErrorExceptionMessageDeserializer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Optional;

@Service
public class UserManagementServiceImpl implements UserManagementService, HttpClientErrorExceptionMessageDeserializer, HttpServerErrorExceptionMessageDeserializer {

    private static Logger log = LoggerFactory.getLogger(UserManagementServiceImpl.class);
    private UserManagementClient userManagementClient;
    private ObjectMapper objectMapper;

    @Autowired
    public UserManagementServiceImpl(UserManagementClient userManagementClient, @Qualifier("primary") ObjectMapper objectMapper) {
        this.userManagementClient = userManagementClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<PersonBasicInfoDto> getPersonById(Long id) throws JsonProcessingException {
        try {
            ResponseEntity<String> response = userManagementClient.getPersonByIdResponseEntity(id);
            if (response.getStatusCode().is2xxSuccessful()) {
                return Optional.ofNullable(objectMapper.readValue(response.getBody(), PersonBasicInfoDto.class));
            }
        } catch (HttpClientErrorException ex) {
            Optional<ApiError> apiErrorOptional = this.deserializeMessage(ex, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));
        } catch (HttpServerErrorException e) {
            Optional<ApiError> apiErrorOptional = this.deserializeServerMessage(e, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));
        }
        return Optional.empty();
    }

    @Override
    public Optional<PersonCompleteRecordDto> getPersonCompleteRecordById(Long id) throws JsonProcessingException {
        try {
            ResponseEntity<String> response = userManagementClient.getPersonCompleteRecordById(id);
            if (response.getStatusCode().is2xxSuccessful()) {
                return Optional.ofNullable(objectMapper.readValue(response.getBody(), PersonCompleteRecordDto.class));
            }
        } catch (HttpClientErrorException ex) {
            Optional<ApiError> apiErrorOptional = this.deserializeMessage(ex, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));
        } catch (HttpServerErrorException e) {
            Optional<ApiError> apiErrorOptional = this.deserializeServerMessage(e, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));
        }
        return Optional.empty();
    }
}
