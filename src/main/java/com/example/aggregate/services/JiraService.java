package com.example.aggregate.services;

import com.example.aggregate.dtos.JiraTicket;

import java.util.List;
import java.util.Optional;

public interface JiraService {
    Optional<JiraTicket> getTicketById(Long id);

    List<JiraTicket> getTicketByName(String name);
}
