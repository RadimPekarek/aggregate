package com.example.aggregate.services;

import com.example.aggregate.clients.JiraServiceClient;
import com.example.aggregate.dtos.JiraTicket;
import com.example.aggregate.wsdl.GetTicketByIdResponse;
import com.example.aggregate.wsdl.GetTicketByNameResponse;
import com.example.aggregate.wsdl.TicketById;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JiraServiceImpl implements JiraService {
    Logger log = LoggerFactory.getLogger(JiraServiceImpl.class);
    private JiraServiceClient jiraServiceClient;

    @Autowired
    public JiraServiceImpl(JiraServiceClient jiraServiceClient) {
        this.jiraServiceClient = jiraServiceClient;
    }


    @Override
    public Optional<JiraTicket> getTicketById(Long id) {
        try {
            GetTicketByIdResponse ticketByIdResponse = jiraServiceClient.getTicketById(id);
            JiraTicket ticket = mapTicketByIdToJiraTicket(ticketByIdResponse.getTicketById());
            return Optional.ofNullable(ticket);
        } catch (Exception ex) {
            log.info("", ex);
        }
        return Optional.empty();
    }

    @Override
    public List<JiraTicket> getTicketByName(String name) {
        try {
            GetTicketByNameResponse ticketByNameResponse = jiraServiceClient.getTicketByName(name);
            return ticketByNameResponse.getTicketsByName()
                    .stream()
                    .map(this::mapTicketByIdToJiraTicket)
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (Exception ex) {
            log.info("", ex);
        }
        return new ArrayList<>();
    }


    private JiraTicket mapTicketByIdToJiraTicket(TicketById ticket) {
        JiraTicket jiraTicket = null;
        if (ticket != null) {
            jiraTicket = new JiraTicket();
            jiraTicket.setId(ticket.getId());
            jiraTicket.setName(ticket.getName());
            jiraTicket.setEmail(ticket.getEmail());
            jiraTicket.setIdPersonCreator(ticket.getIdPersonCreator());
            jiraTicket.setIdPersonAssigned(ticket.getIdPersonAssigned());
            if (ticket.getCreationDatetime() != null)
                jiraTicket.setCreationDateTime(ticket.getCreationDatetime().toGregorianCalendar().toZonedDateTime().toLocalDateTime());
            if (ticket.getTicketCloseDatetime() != null)
                jiraTicket.setTicketCloseTime(ticket.getTicketCloseDatetime().toGregorianCalendar().toZonedDateTime().toLocalDateTime());
        }
        return jiraTicket;
    }
}
