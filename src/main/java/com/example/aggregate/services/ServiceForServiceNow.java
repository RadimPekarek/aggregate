package com.example.aggregate.services;

import com.example.aggregate.clients.objects.RestResponsePage;
import com.example.aggregate.dtos.ServiceNowTicket;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ServiceForServiceNow {

    Optional<ServiceNowTicket> getServiceNowTicketById(Long id) throws JsonProcessingException;

    List<ServiceNowTicket> getServiceNowTicketByName(String name) throws JsonProcessingException;

    RestResponsePage<ServiceNowTicket> getAllTicketsPaginated(Pageable pageable) throws JsonProcessingException;
}
