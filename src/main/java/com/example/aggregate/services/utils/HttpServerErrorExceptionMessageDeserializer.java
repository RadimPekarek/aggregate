package com.example.aggregate.services.utils;

import com.example.aggregate.rest.exceptions.models.ApiError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;
import java.util.Optional;

public interface HttpServerErrorExceptionMessageDeserializer {
    default Optional<ApiError> deserializeServerMessage(HttpServerErrorException ex, ObjectMapper objectMapper) throws JsonProcessingException {
        if (ex != null && ex.getLocalizedMessage() != null) {
            TypeReference<List<ApiError>> typedReference = new TypeReference<List<ApiError>>() {
            };
            List<ApiError> apiErrorList = objectMapper.readValue(ex.getLocalizedMessage().replaceFirst("^[0-9]+ : ", ""), typedReference);
            if (!apiErrorList.isEmpty()) return Optional.ofNullable(apiErrorList.get(0));
        }
        return Optional.empty();
    }
}
