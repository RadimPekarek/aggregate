package com.example.aggregate.services;

import com.example.aggregate.clients.ServiceNowClient;
import com.example.aggregate.clients.objects.RestResponsePage;
import com.example.aggregate.dtos.ServiceNowTicket;
import com.example.aggregate.rest.exceptions.models.ApiError;
import com.example.aggregate.services.exceptions.ServiceLayerForwardingException;
import com.example.aggregate.services.utils.HttpClientErrorExceptionMessageDeserializer;
import com.example.aggregate.services.utils.HttpServerErrorExceptionMessageDeserializer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ServiceForServiceNowImpl implements ServiceForServiceNow, HttpClientErrorExceptionMessageDeserializer, HttpServerErrorExceptionMessageDeserializer {
    private static Logger log = LoggerFactory.getLogger(ServiceForServiceNow.class);
    private ServiceNowClient serviceNowClient;
    private ObjectMapper objectMapper;

    @Autowired
    public ServiceForServiceNowImpl(ServiceNowClient serviceNowClient, @Qualifier("primary") ObjectMapper objectMapper) {
        this.serviceNowClient = serviceNowClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<ServiceNowTicket> getServiceNowTicketById(Long id) throws JsonProcessingException {
        try {
            ResponseEntity<String> response = serviceNowClient.getGetTicketByIdResponseEntity(id);
            if (response.getStatusCode().is2xxSuccessful()) {
                return Optional.ofNullable(objectMapper.readValue(response.getBody(), ServiceNowTicket.class));
            }
        } catch (HttpClientErrorException ex) {
            Optional<ApiError> apiErrorOptional = this.deserializeMessage(ex, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));
        } catch (HttpServerErrorException e) {
            Optional<ApiError> apiErrorOptional = this.deserializeServerMessage(e, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));

        }

        return Optional.empty();
    }

    @Override
    public List<ServiceNowTicket> getServiceNowTicketByName(String name) throws JsonProcessingException {
        try {
            ResponseEntity<String> response = serviceNowClient.getGetTicketByNameResponseEntity(name);
            TypeReference<RestResponsePage<ServiceNowTicket>> typedReference = new TypeReference<RestResponsePage<ServiceNowTicket>>() {
            };
            if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
                return objectMapper.readValue(response.getBody(), typedReference).getContent();
            }
        } catch (HttpClientErrorException ex) {
            Optional<ApiError> apiErrorOptional = this.deserializeMessage(ex, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));
        } catch (HttpServerErrorException e) {
            Optional<ApiError> apiErrorOptional = this.deserializeServerMessage(e, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));

        }
        return new ArrayList<>();
    }

    @Override
    public RestResponsePage<ServiceNowTicket> getAllTicketsPaginated(Pageable pageable) throws JsonProcessingException {
        try {
            ResponseEntity<String> response = serviceNowClient.getGetTicketsResponseEntity(pageable);
            TypeReference<RestResponsePage<ServiceNowTicket>> typedReference = new TypeReference<RestResponsePage<ServiceNowTicket>>() {
            };
            if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
                return objectMapper.readValue(response.getBody(), typedReference);
            }
        } catch (HttpClientErrorException ex) {
            Optional<ApiError> apiErrorOptional = this.deserializeMessage(ex, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));
        } catch (HttpServerErrorException e) {
            Optional<ApiError> apiErrorOptional = this.deserializeServerMessage(e, objectMapper);
            throw new ServiceLayerForwardingException(apiErrorOptional.orElseThrow(RuntimeException::new));

        }
        return null;
    }
}
