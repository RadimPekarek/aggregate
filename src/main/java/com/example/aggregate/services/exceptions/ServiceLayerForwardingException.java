package com.example.aggregate.services.exceptions;

import com.example.aggregate.rest.exceptions.models.ApiError;

public class ServiceLayerForwardingException extends RuntimeException {
    private ApiError apiError;

    public ServiceLayerForwardingException(ApiError apiError) {
        this.apiError = apiError;
    }

    public ServiceLayerForwardingException(String message, ApiError apiError) {
        super(message);
        this.apiError = apiError;
    }

    public ServiceLayerForwardingException(String message, Throwable cause, ApiError apiError) {
        super(message, cause);
        this.apiError = apiError;
    }

    public ServiceLayerForwardingException(Throwable cause, ApiError apiError) {
        super(cause);
        this.apiError = apiError;
    }

    public ServiceLayerForwardingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ApiError apiError) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.apiError = apiError;
    }

    public ApiError getApiError() {
        return apiError;
    }

    public void setApiError(ApiError apiError) {
        this.apiError = apiError;
    }
}
