package com.example.aggregate.clients;

import com.example.aggregate.clients.objects.RestResponsePage;
import com.example.aggregate.dtos.ServiceNowTicket;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;


@Component
public class ServiceNowClient {
    Logger log = LoggerFactory.getLogger(ServiceNowClient.class);
    @Value("${service.now.protocol}")
    private String protocol;
    @Value("${service.now.host}")
    private String host;
    @Value("${service.now.port}")
    private String port;

    private String commonPart;
    private String getTicketById;
    private String getTicketByName;
    private String getTickets;

    private RestTemplate restTemplate;
    private ObjectMapper objectMapper;

    @Autowired
    public ServiceNowClient(RestTemplate restTemplate, @Qualifier("primary") ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void init() {
        commonPart = protocol + "://" + host + ":" + port + "/";
        getTicketById = commonPart + "tickets/";
        getTicketByName = commonPart + "tickets?name=";
        getTickets = commonPart + "tickets";
    }


    public ResponseEntity<String> getGetTicketByIdResponseEntity(Long id) {
        return restTemplate.getForEntity(getTicketById + id, String.class);
    }

    public ServiceNowTicket getGetTicketById(Long id) {
        return restTemplate.getForObject(getTicketById + id, ServiceNowTicket.class);
    }

    public ResponseEntity<String> getGetTicketByNameResponseEntity(String name) {
        return restTemplate.getForEntity(getTicketByName + name, String.class);
    }

    public RestResponsePage<ServiceNowTicket> getGetTicketByName(String name) {
        ParameterizedTypeReference<RestResponsePage<ServiceNowTicket>> responseType = new ParameterizedTypeReference<RestResponsePage<ServiceNowTicket>>() {
        };
        ResponseEntity<RestResponsePage<ServiceNowTicket>> result = restTemplate.exchange(getTicketByName + name, HttpMethod.GET, null/*httpEntity*/, responseType);
        return result.getBody();
    }

    public RestResponsePage<ServiceNowTicket> getGetTickets() {
        ParameterizedTypeReference<RestResponsePage<ServiceNowTicket>> responseType = new ParameterizedTypeReference<RestResponsePage<ServiceNowTicket>>() {
        };
        ResponseEntity<RestResponsePage<ServiceNowTicket>> result = restTemplate.exchange(getTickets, HttpMethod.GET, null/*httpEntity*/, responseType);
        return result.getBody();
    }

    public ResponseEntity<String> getGetTicketsResponseEntity(Pageable pageable) {
        String queryPart = convertPageableToQueryString(pageable);
        return restTemplate.getForEntity(getTickets + queryPart, String.class);
    }

    private String convertPageableToQueryString(Pageable pageable) {
        int page = pageable.getPageNumber();
        int size = pageable.getPageSize();
        Sort sort = pageable.getSort();
        StringBuilder builder = new StringBuilder();
        sort.get().forEach(order ->
                builder.append("&sort=")
                        .append(order.getProperty())
                        .append(",")
                        .append(order.getDirection().toString().toLowerCase()));
        return "?page=" + page + "&size=" + size + builder.toString();
    }
}
