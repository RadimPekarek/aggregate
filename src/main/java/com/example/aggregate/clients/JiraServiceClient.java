package com.example.aggregate.clients;

import com.example.aggregate.wsdl.GetTicketByIdRequest;
import com.example.aggregate.wsdl.GetTicketByIdResponse;
import com.example.aggregate.wsdl.GetTicketByNameRequest;
import com.example.aggregate.wsdl.GetTicketByNameResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.annotation.PostConstruct;

@Component
public class JiraServiceClient extends WebServiceGatewaySupport {
    @Value("${jira.protocol}")
    private String protocol;
    @Value("${jira.host}")
    private String host;
    @Value("${jira.port}")
    private String port;
    @Value("${jira.wsdl.name}")
    private String wsdlName;
    @Value("${jira.namespace.uri}")
    private String nameSpaceUri;

    private String commonPart;
    private String ticketsEndpoint;

    private static final Logger log = LoggerFactory.getLogger(JiraServiceClient.class);
    private Jaxb2Marshaller marshaller;

    @PostConstruct
    private void init() {
        commonPart = protocol + "://" + host + ":" + port + "/ws";
        ticketsEndpoint = commonPart + wsdlName;
    }

    public JiraServiceClient(Jaxb2Marshaller marshaller) {
        this.marshaller = marshaller;
        this.setDefaultUri(commonPart);
        this.setMarshaller(marshaller);
        this.setUnmarshaller(marshaller);
    }

    public GetTicketByIdResponse getTicketById(Long id) {
        GetTicketByIdRequest request = new GetTicketByIdRequest();
        request.setId(id);
        return (GetTicketByIdResponse) getWebServiceTemplate()
                .marshalSendAndReceive(ticketsEndpoint, request,
                        new SoapActionCallback(nameSpaceUri + "/GetTicketByIdRequest"));
    }

    public GetTicketByNameResponse getTicketByName(String name) {
        GetTicketByNameRequest request = new GetTicketByNameRequest();
        request.setName(name);
        return (GetTicketByNameResponse) getWebServiceTemplate()
                .marshalSendAndReceive(ticketsEndpoint, request,
                        new SoapActionCallback(nameSpaceUri + "/GetTicketByNameRequest"));
    }

}
