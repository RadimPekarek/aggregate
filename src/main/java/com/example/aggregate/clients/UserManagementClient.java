package com.example.aggregate.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Component
public class UserManagementClient {

    Logger log = LoggerFactory.getLogger(ServiceNowClient.class);
    @Value("${user.management.protocol}")
    private String protocol;
    @Value("${user.management.host}")
    private String host;
    @Value("${user.management.port}")
    private String port;

    private String commonPart;
    private String getPersonById;
    private String getPersonsRoles;
    private String getPersonsCompleteRecord;


    private RestTemplate restTemplate;
    private ObjectMapper objectMapper;

    @Autowired
    public UserManagementClient(@Qualifier("third") RestTemplate restTemplate, @Qualifier("primary") ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void init() {
        commonPart = protocol + "://" + host + ":" + port + "/";
        getPersonById = commonPart + "persons/";
        getPersonsRoles = "/roles";
        getPersonsCompleteRecord = "/full";
    }

    public ResponseEntity<String> getPersonByIdResponseEntity(Long id) {
        HttpEntity<String> request = new HttpEntity<>(httpHeaders());
        return restTemplate.exchange(getPersonById + id, HttpMethod.GET, request, String.class);
    }


    public ResponseEntity<String> getPersonCompleteRecordById(Long id) {
        HttpEntity<String> request = new HttpEntity<>(httpHeaders());
        return restTemplate.exchange(getPersonById + id + getPersonsCompleteRecord, HttpMethod.GET, request, String.class);

    }

    private HttpHeaders httpHeaders() {
        String plainCreds = "radek.kruta@seznam.cz:password";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);
        return headers;

    }

}
