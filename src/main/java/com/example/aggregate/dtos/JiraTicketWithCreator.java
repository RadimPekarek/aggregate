package com.example.aggregate.dtos;

import java.util.Objects;

public class JiraTicketWithCreator {

    private JiraTicket ticket;
    private PersonCompleteRecordDto creator;

    public JiraTicketWithCreator() {
    }

    public JiraTicketWithCreator(JiraTicket ticket, PersonCompleteRecordDto creator) {
        this.ticket = ticket;
        this.creator = creator;
    }

    public JiraTicket getTicket() {
        return ticket;
    }

    public void setTicket(JiraTicket ticket) {
        this.ticket = ticket;
    }

    public PersonCompleteRecordDto getCreator() {
        return creator;
    }

    public void setCreator(PersonCompleteRecordDto creator) {
        this.creator = creator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JiraTicketWithCreator)) return false;
        JiraTicketWithCreator that = (JiraTicketWithCreator) o;
        return Objects.equals(getTicket(), that.getTicket()) &&
                Objects.equals(getCreator(), that.getCreator());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTicket(), getCreator());
    }

    @Override
    public String toString() {
        return "JiraTicketWithCreator{" +
                "ticket=" + ticket +
                ", creator=" + creator +
                '}';
    }
}
