package com.example.aggregate.dtos;

import java.util.Objects;

public class ServiceNowTicketWithBothPersons extends ServiceNowTicketWithCreator {
    private PersonCompleteRecordDto assignee;

    public ServiceNowTicketWithBothPersons() {
        super();
    }

    public ServiceNowTicketWithBothPersons(ServiceNowTicket ticket, PersonCompleteRecordDto personCompleteRecord, PersonCompleteRecordDto assignee) {
        super(ticket, personCompleteRecord);
        this.assignee = assignee;
    }

    public PersonCompleteRecordDto getAssignee() {
        return assignee;
    }

    public void setAssignee(PersonCompleteRecordDto assignee) {
        this.assignee = assignee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServiceNowTicketWithBothPersons)) return false;
        if (!super.equals(o)) return false;
        ServiceNowTicketWithBothPersons that = (ServiceNowTicketWithBothPersons) o;
        return Objects.equals(getAssignee(), that.getAssignee()) && Objects.equals(super.getTicket(), that.getTicket())
                && Objects.equals(super.getCreator(), that.getCreator());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAssignee());
    }

    @Override
    public String toString() {
        return "ServiceNowTicketWithBothPersons{ ticket=" + super.getTicket()
                + " creator=" + super.getCreator()
                + " assignee=" + assignee +
                '}';
    }
}
