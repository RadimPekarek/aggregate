package com.example.aggregate.dtos;

import java.util.Objects;

public class JiraTicketWithAssignee {
    private JiraTicket ticket;
    private PersonCompleteRecordDto assignee;

    public JiraTicketWithAssignee() {
    }

    public JiraTicketWithAssignee(JiraTicket ticket, PersonCompleteRecordDto assignee) {
        this.ticket = ticket;
        this.assignee = assignee;
    }

    public JiraTicket getTicket() {
        return ticket;
    }

    public void setTicket(JiraTicket ticket) {
        this.ticket = ticket;
    }

    public PersonCompleteRecordDto getAssignee() {
        return assignee;
    }

    public void setAssignee(PersonCompleteRecordDto assignee) {
        this.assignee = assignee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JiraTicketWithAssignee)) return false;
        JiraTicketWithAssignee that = (JiraTicketWithAssignee) o;
        return Objects.equals(getTicket(), that.getTicket()) &&
                Objects.equals(getAssignee(), that.getAssignee());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTicket(), getAssignee());
    }

    @Override
    public String toString() {
        return "JiraTicketWithAssignee{" +
                "ticket=" + ticket +
                ", assignee=" + assignee +
                '}';
    }
}
