package com.example.aggregate.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Objects;

@ApiModel(value = "JiraTicket", description = "Information about ticket.")
public class JiraTicket {

    @ApiModelProperty(value = "Ticket id.", example = "1", notes = "The database generated ticket ID")
    private Long id;
    @ApiModelProperty(value = "Ticket name", example = "rolling incident")
    private String name;
    @ApiModelProperty(value = "Ticket email.", example = "radek.kruta@seznam.czt", notes = "The email of person who created the ticket")
    private String email;
    @ApiModelProperty(value = "Id of person.", example = "1", notes = "The id of person who created the ticket")
    private Long idPersonCreator;
    @ApiModelProperty(value = "Id of person.", example = "2", notes = "The id of person to whom the ticket was assigned")
    private Long idPersonAssigned;
    @ApiModelProperty(value = "Ticket creation timestamp.", example = "2020-02-19T11:56:13.89608", notes = "The timestamp when the ticket was created")
    private LocalDateTime creationDateTime;
    @ApiModelProperty(value = "Ticket closed timestamp.", example = "2020-02-19T11:56:13.89608", notes = "The timestamp when the ticket was closed")
    private LocalDateTime ticketCloseTime;

    public JiraTicket() {
    }

    public JiraTicket(Long id, String name, String email, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime, LocalDateTime ticketCloseTime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
        this.ticketCloseTime = ticketCloseTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public LocalDateTime getTicketCloseTime() {
        return ticketCloseTime;
    }

    public void setTicketCloseTime(LocalDateTime ticketCloseTime) {
        this.ticketCloseTime = ticketCloseTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JiraTicket)) return false;
        JiraTicket that = (JiraTicket) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getIdPersonCreator(), that.getIdPersonCreator()) &&
                Objects.equals(getIdPersonAssigned(), that.getIdPersonAssigned()) &&
                Objects.equals(getCreationDateTime(), that.getCreationDateTime()) &&
                Objects.equals(getTicketCloseTime(), that.getTicketCloseTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getEmail(), getIdPersonCreator(), getIdPersonAssigned(), getCreationDateTime(), getTicketCloseTime());
    }

    @Override
    public String toString() {
        return "JiraTicket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDateTime=" + creationDateTime +
                ", ticketCloseTime=" + ticketCloseTime +
                '}';
    }
}
