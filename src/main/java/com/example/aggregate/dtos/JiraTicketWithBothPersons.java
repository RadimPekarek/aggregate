package com.example.aggregate.dtos;

import java.util.Objects;

public class JiraTicketWithBothPersons {

    private JiraTicket ticket;
    private PersonCompleteRecordDto creator;
    private PersonCompleteRecordDto assignee;

    public JiraTicketWithBothPersons() {
    }

    public JiraTicketWithBothPersons(JiraTicket ticket, PersonCompleteRecordDto creator, PersonCompleteRecordDto assignee) {
        this.ticket = ticket;
        this.creator = creator;
        this.assignee = assignee;
    }

    public JiraTicket getTicket() {
        return ticket;
    }

    public void setTicket(JiraTicket ticket) {
        this.ticket = ticket;
    }

    public PersonCompleteRecordDto getCreator() {
        return creator;
    }

    public void setCreator(PersonCompleteRecordDto creator) {
        this.creator = creator;
    }

    public PersonCompleteRecordDto getAssignee() {
        return assignee;
    }

    public void setAssignee(PersonCompleteRecordDto assignee) {
        this.assignee = assignee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JiraTicketWithBothPersons)) return false;
        JiraTicketWithBothPersons that = (JiraTicketWithBothPersons) o;
        return Objects.equals(getTicket(), that.getTicket()) &&
                Objects.equals(getCreator(), that.getCreator()) &&
                Objects.equals(getAssignee(), that.getAssignee());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTicket(), getCreator(), getAssignee());
    }

    @Override
    public String toString() {
        return "JiraTicketWithBothPersons{" +
                "ticket=" + ticket +
                ", creator=" + creator +
                ", assignee=" + assignee +
                '}';
    }
}
