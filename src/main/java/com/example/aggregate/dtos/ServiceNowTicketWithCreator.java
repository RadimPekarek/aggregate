package com.example.aggregate.dtos;

import java.util.Objects;

public class ServiceNowTicketWithCreator {
    private ServiceNowTicket ticket;
    private PersonCompleteRecordDto creator;

    public ServiceNowTicketWithCreator() {
    }

    public ServiceNowTicketWithCreator(ServiceNowTicket ticket, PersonCompleteRecordDto personCompleteRecord) {
        this.ticket = ticket;
        this.creator = personCompleteRecord;
    }

    public ServiceNowTicket getTicket() {
        return ticket;
    }

    public void setTicket(ServiceNowTicket ticket) {
        this.ticket = ticket;
    }

    public PersonCompleteRecordDto getCreator() {
        return creator;
    }

    public void setCreator(PersonCompleteRecordDto creator) {
        this.creator = creator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServiceNowTicketWithCreator)) return false;
        ServiceNowTicketWithCreator that = (ServiceNowTicketWithCreator) o;
        return Objects.equals(getTicket(), that.getTicket()) &&
                Objects.equals(getCreator(), that.getCreator());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTicket(), getCreator());
    }

    @Override
    public String toString() {
        return "ServiceNowTicketWithCreator{" +
                "ticket=" + ticket +
                ", personCompleteRecord=" + creator +
                '}';
    }
}
