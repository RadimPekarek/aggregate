package com.example.aggregate.dtos;

import java.util.Objects;

public class ServiceNowTicketWithAssignee {

    private ServiceNowTicket ticket;
    private PersonCompleteRecordDto assignee;

    public ServiceNowTicketWithAssignee() {
    }

    public ServiceNowTicketWithAssignee(ServiceNowTicket ticket, PersonCompleteRecordDto assignee) {
        this.ticket = ticket;
        this.assignee = assignee;
    }

    public ServiceNowTicket getTicket() {
        return ticket;
    }

    public void setTicket(ServiceNowTicket ticket) {
        this.ticket = ticket;
    }

    public PersonCompleteRecordDto getAssignee() {
        return assignee;
    }

    public void setAssignee(PersonCompleteRecordDto assignee) {
        this.assignee = assignee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServiceNowTicketWithAssignee)) return false;
        ServiceNowTicketWithAssignee that = (ServiceNowTicketWithAssignee) o;
        return Objects.equals(getTicket(), that.getTicket()) &&
                Objects.equals(getAssignee(), that.getAssignee());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTicket(), getAssignee());
    }

    @Override
    public String toString() {
        return "ServiceNowTicketWithAssignee{" +
                "ticket=" + ticket +
                ", assignee=" + assignee +
                '}';
    }
}
