package com.example.aggregate.jms;

import com.example.aggregate.dtos.ServiceNowTicket;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;


@Component
public class JmsMessageListener {
    private static Logger log = LoggerFactory.getLogger(JmsMessageListener.class);
    @Autowired
    @Qualifier("primary")
    private ObjectMapper objectMapper;

    @JmsListener(destination = "radim.tickets.que")
    public void subscribeTicket(Message ticket) throws JMSException, JsonProcessingException {
        String messageData = null;
        if (ticket instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) ticket;
            messageData = textMessage.getText();
            log.info("received TextMessage: {}", messageData);
            ServiceNowTicket ticketConverted = objectMapper.readValue(messageData, ServiceNowTicket.class);
            log.info("converted to ServiceNowTicket: {}", ticketConverted);
        }
    }


}
