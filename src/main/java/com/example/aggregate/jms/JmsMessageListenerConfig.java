package com.example.aggregate.jms;

import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;


@EnableJms
@Configuration
public class JmsMessageListenerConfig {

}
