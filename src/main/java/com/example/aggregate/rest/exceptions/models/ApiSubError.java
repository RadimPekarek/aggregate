package com.example.aggregate.rest.exceptions.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = ApiValidationError.class)
public abstract class ApiSubError {
    public ApiSubError() {
    }
}
