package com.example.aggregate.rest.exceptions.restexceptions;

import com.example.aggregate.rest.exceptions.models.ApiError;

public class RestForwardedException extends RuntimeException {
    private ApiError apiError;

    public RestForwardedException(ApiError apiError) {
        this.apiError = apiError;
    }

    public RestForwardedException(String message, ApiError apiError) {
        super(message);
        this.apiError = apiError;
    }

    public RestForwardedException(String message, Throwable cause, ApiError apiError) {
        super(message, cause);
        this.apiError = apiError;
    }

    public RestForwardedException(Throwable cause, ApiError apiError) {
        super(cause);
        this.apiError = apiError;
    }

    public RestForwardedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ApiError apiError) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.apiError = apiError;
    }

    public ApiError getApiError() {
        return apiError;
    }

    public void setApiError(ApiError apiError) {
        this.apiError = apiError;
    }
}
