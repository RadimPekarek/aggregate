package com.example.aggregate.rest.exceptions.config;

import com.example.aggregate.rest.exceptions.models.ApiErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebErrorConfiguration {

    @Bean
    public ErrorAttributes errorAttributes() {
        return new ApiErrorAttributes(true);
    }

}
