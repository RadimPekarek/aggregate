package com.example.aggregate.rest.exceptions.restexceptions;

public class TicketNotCreatedException extends RuntimeException {

    public TicketNotCreatedException() {
    }

    public TicketNotCreatedException(String message) {
        super(message);
    }

    public TicketNotCreatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TicketNotCreatedException(Throwable cause) {
        super(cause);
    }

    public TicketNotCreatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
