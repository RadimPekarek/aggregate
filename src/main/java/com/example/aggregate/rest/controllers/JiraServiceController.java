package com.example.aggregate.rest.controllers;

import com.example.aggregate.dtos.*;
import com.example.aggregate.rest.exceptions.restexceptions.PersonNotFoundException;
import com.example.aggregate.rest.exceptions.restexceptions.TicketNotFoundException;
import com.example.aggregate.services.JiraService;
import com.example.aggregate.services.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping("/jira/tickets")
@Api(value = "/jira/tickets", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class JiraServiceController {

    private static Logger log = LoggerFactory.getLogger(JiraServiceController.class);
    private JiraService ticketService;
    private UserManagementService userManagementService;

    @Autowired
    public JiraServiceController(JiraService ticketService, UserManagementService userManagementService) {
        this.ticketService = ticketService;
        this.userManagementService = userManagementService;
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get a jira ticket by Id",
            response = JiraTicket.class,
            nickname = "getTicketById",
            produces = "application/json"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved ticket"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    public Object getTicketById(
            @ApiParam(name = "id", value = "Jira ticket id to retrieve ticket", required = true)
            @PathVariable("id") @Min(1) Long id,
            @ApiParam(name = "withCreator", value = "Sets if PersonCreatorInfo should be retrieved", required = false)
            @RequestParam(name = "withCreator", required = false, defaultValue = "false") boolean withCreator,
            @ApiParam(name = "withAssignee", value = "Sets if PersonAssignedInfo should be retrieved", required = false)
            @RequestParam(name = "withAssignee", required = false, defaultValue = "false") boolean withAssignee
    ) {
        try {
            JiraTicket ticket = ticketService.getTicketById(id).orElseThrow(() -> new TicketNotFoundException("No ticket with id " + id + " found!"));
            if (withCreator && withAssignee) {
                PersonCompleteRecordDto creator = userManagementService.getPersonCompleteRecordById(ticket.getIdPersonCreator()).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", ticket.getIdPersonCreator().toString()));
                PersonCompleteRecordDto assignee = userManagementService.getPersonCompleteRecordById(ticket.getIdPersonAssigned()).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", ticket.getIdPersonAssigned().toString()));
                return new JiraTicketWithBothPersons(ticket, creator, assignee);
            }
            if (withCreator) {
                PersonCompleteRecordDto creator = userManagementService.getPersonCompleteRecordById(ticket.getIdPersonCreator()).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", ticket.getIdPersonCreator().toString()));
                return new JiraTicketWithCreator(ticket, creator);
            }
            if (withAssignee) {
                PersonCompleteRecordDto assignee = userManagementService.getPersonCompleteRecordById(ticket.getIdPersonAssigned()).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", ticket.getIdPersonAssigned().toString()));
                return new JiraTicketWithAssignee(ticket, assignee);
            }
            return ticket;
        } catch (
                JsonProcessingException exception) {
            throw new HttpMessageNotReadableException("Something went wrong", exception);
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved ticket"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    public List<JiraTicket> getTicketsByName(
            @ApiParam(name = "name", value = "Jira ticket name to retrieve tickets", required = true)
            @RequestParam("name") @NotEmpty() String name
    ) {
        return ticketService.getTicketByName(name);
    }

}
