package com.example.aggregate.rest.controllers;


import com.example.aggregate.dtos.*;
import com.example.aggregate.rest.ApiPageableSwagger;
import com.example.aggregate.rest.exceptions.restexceptions.PersonNotFoundException;
import com.example.aggregate.rest.exceptions.restexceptions.RestForwardedException;
import com.example.aggregate.rest.exceptions.restexceptions.TicketNotFoundException;
import com.example.aggregate.services.ServiceForServiceNow;
import com.example.aggregate.services.UserManagementService;
import com.example.aggregate.services.exceptions.ServiceLayerForwardingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;

@RestController
@RequestMapping("/service-now/tickets")
@Api(value = "/service-now/tickets", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class ServiceNowTicketController {

    private static Logger log = LoggerFactory.getLogger(ServiceNowTicketController.class);
    private ServiceForServiceNow ticketService;
    private UserManagementService userManagementService;


    @Autowired
    public ServiceNowTicketController(ServiceForServiceNow ticketService, UserManagementService userManagementService) {
        this.ticketService = ticketService;
        this.userManagementService = userManagementService;

    }


    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get a servicenow ticket by Id",
            response = ServiceNowTicket.class,
            nickname = "getTicketById",
            produces = "application/json"
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved ticket"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    public Object getTicketById(
            @ApiParam(name = "id", value = "Ticket id to retrieve ticket", required = true)
            @PathVariable("id") @Min(1) Long id,
            @ApiParam(name = "withCreator", value = "Sets if PersonCreatorInfo should be retrieved", required = false)
            @RequestParam(name = "withCreator", required = false, defaultValue = "false") boolean withCreator,
            @ApiParam(name = "withAssignee", value = "Sets if PersonAssignedInfo should be retrieved", required = false)
            @RequestParam(name = "withAssignee", required = false, defaultValue = "false") boolean withAssignee
    ) {
        try {
            ServiceNowTicket ticket = ticketService.getServiceNowTicketById(id).orElseThrow(() -> new TicketNotFoundException("No ticket with id " + id + " found!"));
            if (withCreator && withAssignee) {
                PersonCompleteRecordDto creator = userManagementService.getPersonCompleteRecordById(ticket.getIdPersonCreator()).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", ticket.getIdPersonCreator().toString()));
                PersonCompleteRecordDto assignee = userManagementService.getPersonCompleteRecordById(ticket.getIdPersonAssigned()).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", ticket.getIdPersonAssigned().toString()));
                return new ServiceNowTicketWithBothPersons(ticket, creator, assignee);
            }
            if (withCreator) {
                PersonCompleteRecordDto creator = userManagementService.getPersonCompleteRecordById(ticket.getIdPersonCreator()).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", ticket.getIdPersonCreator().toString()));
                return new ServiceNowTicketWithCreator(ticket, creator);
            }
            if (withAssignee) {
                PersonCompleteRecordDto assignee = userManagementService.getPersonCompleteRecordById(ticket.getIdPersonAssigned()).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", ticket.getIdPersonAssigned().toString()));
                return new ServiceNowTicketWithAssignee(ticket, assignee);
            }

            return ticket;
        } catch (ServiceLayerForwardingException ex) {
            throw new RestForwardedException(ex.getApiError());
        } catch (JsonProcessingException exception) {
            throw new HttpMessageNotReadableException("Something went wrong", exception);
        }
    }

    @ApiOperation(
            httpMethod = "GET",
            value = "Get All Tickets.",
            response = ServiceNowTicket.class,
            responseContainer = "Page",
            nickname = "getAllTickets",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "The requested resource was not found.")
    })
    @ApiPageableSwagger
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public Page<ServiceNowTicket> getAllTickets(Pageable pageable) {
        try {
            return ticketService.getAllTicketsPaginated(pageable);
        } catch (ServiceLayerForwardingException ex) {
            throw new RestForwardedException(ex.getApiError());
        } catch (JsonProcessingException exception) {
            throw new HttpMessageNotReadableException("Something went wrong", exception);
        }

    }
}
