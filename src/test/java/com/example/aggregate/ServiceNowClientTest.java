package com.example.aggregate;

import com.example.aggregate.clients.ServiceNowClient;
import com.example.aggregate.clients.objects.RestResponsePage;
import com.example.aggregate.dtos.ServiceNowTicket;
import com.example.aggregate.rest.exceptions.models.ApiError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;


@RunWith(SpringRunner.class)
@DirtiesContext
@SpringBootTest(classes = AggregateApplication.class)
public class ServiceNowClientTest {
    @Value("${service.now.protocol}")
    private String protocol;
    @Value("${service.now.host}")
    private String host;
    @Value("${service.now.port}")
    private String port;

    private String commonPart;
    private String getTicketById;
    private String getTicketByName;
    private String getTickets;

    private static Logger log = LoggerFactory.getLogger(ServiceNowClientTest.class);

    @Autowired
    ServiceNowClient serviceNowClient;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    @Qualifier("primary")
    private ObjectMapper mapper;


    private MockRestServiceServer mockServer;

    private boolean isRunning;

    @Before
    public void init() {
        setCommonUrlParts();
        try {
            if (restTemplate.getForEntity(getTicketById + 1, String.class).getStatusCode().is2xxSuccessful())
                isRunning = true;

        } catch (Exception ex) {
            log.info("ServiceNow is not running. Switching to mocking mode...");
            mockServer = MockRestServiceServer.createServer(restTemplate);
        }
    }

    @Test
    @DirtiesContext
    public void getTicketByIdResponseEntity() throws JsonProcessingException {
        if (isRunning) {
            ResponseEntity<String> response = serviceNowClient.getGetTicketByIdResponseEntity(1L);
            log.info("{}", mapper.readValue(response.getBody(), ServiceNowTicket.class));
            assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
            assertFalse(response.getBody().isEmpty());
        }
    }

    @Test
    @DirtiesContext
    public void getTicketByIdResponseEntityException() throws JsonProcessingException {
        TypeReference<List<ApiError>> typedReference = new TypeReference<List<ApiError>>() {
        };
        if (isRunning) {
            try {
                ResponseEntity<String> response = serviceNowClient.getGetTicketByIdResponseEntity(0L);
            } catch (HttpClientErrorException ex) {
                log.info("{}", ex.getLocalizedMessage());
            }
        }
    }

    @Test
    @DirtiesContext
    public void getTicketById() throws URISyntaxException, JsonProcessingException {
        if (isRunning) {
            try {
                ServiceNowTicket ticket = serviceNowClient.getGetTicketById(1L);
                log.info("ticket {}", ticket);
                assertNotNull(ticket);
            } catch (HttpClientErrorException exception) {
                log.info("{}", exception);
            }

        } else {
            ServiceNowTicket ticket = new ServiceNowTicket(1L, "accounting incident", "barbora.nacata@seznam.cz", 4L, 3L, LocalDateTime.now(Clock.systemUTC()));
            prepareMockServer(getTicketById + 1, ticket);
            ServiceNowTicket serviceNowTicket = serviceNowClient.getGetTicketById(1L);
            mockServer.verify();
            assertEquals(ticket, serviceNowTicket);
        }
    }

    @Test
    @DirtiesContext
    public void getTicketByNameResponseEntity() {
        if (isRunning) {
            ResponseEntity<String> response = serviceNowClient.getGetTicketByNameResponseEntity("rolling incident");
            log.info("json {}", response);
            assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
            assertFalse(Objects.requireNonNull(response.getBody()).isEmpty());
        }
    }

    @Test
    @DirtiesContext
    public void getTicketByName() throws URISyntaxException, JsonProcessingException {
        if (isRunning) {
            RestResponsePage<ServiceNowTicket> ticket = serviceNowClient.getGetTicketByName("rolling incident");
            log.info("ticket {}", ticket.getContent());
            assertTrue(ticket.hasContent());
        } else {
            ServiceNowTicket ticket = new ServiceNowTicket(1L, "accounting incident", "barbora.nacata@seznam.cz", 4L, 3L, LocalDateTime.now(Clock.systemUTC()));
            prepareMockServer(getTicketByName + "accounting%20incident", ticket);
            RestResponsePage<ServiceNowTicket> serviceNowTicket = serviceNowClient.getGetTicketByName("accounting incident");
            mockServer.verify();
            assertEquals(ticket, serviceNowTicket.getContent().get(0));

        }
    }


    private void prepareMockServer(String uri, ServiceNowTicket ticket) throws URISyntaxException, JsonProcessingException {
        mockServer.expect(ExpectedCount.manyTimes(),
                requestTo(new URI(uri)))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(ticket))
                );

    }

    private void setCommonUrlParts() {
        commonPart = protocol + "://" + host + ":" + port + "/";
        getTicketById = commonPart + "tickets/";
        getTicketByName = commonPart + "tickets?name=";
        getTickets = commonPart + "ticket/all";
    }

}

