package com.example.aggregate;

import com.example.aggregate.clients.ServiceNowClient;
import com.example.aggregate.clients.objects.RestResponsePage;
import com.example.aggregate.dtos.ServiceNowTicket;
import com.example.aggregate.services.ServiceForServiceNowImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AggregateApplication.class)
public class ServiceForServiceNowTest {

    private static Logger log = LoggerFactory.getLogger(ServiceForServiceNowTest.class);

    @MockBean
    private ServiceNowClient serviceNowClient;
    @Autowired
    private ServiceForServiceNowImpl serviceForServiceNow;
    @Autowired
    @Qualifier("primary")
    private ObjectMapper objectMapper;


    @Test
    public void getServiceNowTicketById() throws JsonProcessingException {
        String jsonTicket = "{\"id\":1,\"name\":\"accounting incident\",\"email\":\"barbora.nacata@seznam.cz\",\"idPersonCreator\":4,\"idPersonAssigned\":3,\"creationDateTime\":\"2020-02-18T13:56:13.89608\"}";
        ResponseEntity<String> response = new ResponseEntity<>(jsonTicket, HttpStatus.OK);
        when(serviceNowClient.getGetTicketByIdResponseEntity(1L)).thenReturn(response);
        Optional<ServiceNowTicket> ticket = Optional.ofNullable(objectMapper.readValue(response.getBody(), ServiceNowTicket.class));
        assertTrue(ticket.isPresent());
        assertEquals(ticket, serviceForServiceNow.getServiceNowTicketById(1L));
    }

    @Test
    public void getServiceNowTicketByName() throws JsonProcessingException {
        String jsonTicket = "{\"content\":[{\"id\":2,\"name\":\"rolling incident\",\"email\":\"radek.kruta@seznam.cz\",\"idPersonCreator\":1,\"idPersonAssigned\":2,\"creationDateTime\":\"2020-02-19T11:56:13.89608\"}],\"pageable\":{\"sort\":{\"sorted\":false,\"unsorted\":true,\"empty\":true},\"offset\":0,\"pageSize\":20,\"pageNumber\":0,\"unpaged\":false,\"paged\":true},\"last\":true,\"totalPages\":1,\"totalElements\":1,\"size\":20,\"number\":0,\"sort\":{\"sorted\":false,\"unsorted\":true,\"empty\":true},\"numberOfElements\":1,\"first\":true,\"empty\":false}";
        ResponseEntity<String> response = new ResponseEntity<>(jsonTicket, HttpStatus.OK);
        when(serviceNowClient.getGetTicketByNameResponseEntity("rolling incident")).thenReturn(response);
        TypeReference<RestResponsePage<ServiceNowTicket>> typedReference = new TypeReference<RestResponsePage<ServiceNowTicket>>() {
        };
        List<ServiceNowTicket> ticket = objectMapper.readValue(response.getBody(), typedReference).getContent();

        assertFalse(ticket.isEmpty());
        assertEquals(ticket, serviceForServiceNow.getServiceNowTicketByName("rolling incident"));
    }

}
